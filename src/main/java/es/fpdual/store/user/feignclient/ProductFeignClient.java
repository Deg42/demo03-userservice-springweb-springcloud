package es.fpdual.store.user.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import es.fpdual.store.user.model.Product;

@FeignClient(name = "product-service", url = "http://localhost:8081/products")
public interface ProductFeignClient {

    @PostMapping
    public Product createProduct(@RequestBody Product product);

}
