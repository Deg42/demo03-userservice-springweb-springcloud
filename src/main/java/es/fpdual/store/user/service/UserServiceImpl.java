package es.fpdual.store.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import es.fpdual.store.user.entity.User;
import es.fpdual.store.user.feignclient.ProductFeignClient;
import es.fpdual.store.user.model.Product;
import es.fpdual.store.user.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    ProductFeignClient productFeignClient;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<Product> getProducts(Long userId) {
        List<Product> products = restTemplate.getForObject("http://localhost:8081/products/user/" + userId, List.class);
        return products;
    }

    @Override
    public Product createProduct(Long userId, Product product) {
        product.setUserId(userId);
        Product newProduct = productFeignClient.createProduct(product);
        return newProduct;
    }

    @Override
    public List<User> listAllUser() {
        return userRepository.findAll();
    }

    @Override
    public User getUser(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public User createUser(User user) {
        User userDB = userRepository.findByEmail(user.getEmail());
        if (userDB != null) {
            return userDB;
        }

        User newUser = userRepository.save(user);
        return newUser;
    }

}
