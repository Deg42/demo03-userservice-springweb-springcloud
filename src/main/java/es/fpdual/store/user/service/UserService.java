package es.fpdual.store.user.service;

import java.util.List;

import es.fpdual.store.user.entity.User;
import es.fpdual.store.user.model.Product;

public interface UserService {

    public List<Product> getProducts(Long userId);

    public Product createProduct(Long userId, Product product);

    public List<User> listAllUser();

    public User getUser(Long id);

    public User createUser(User user);

}
