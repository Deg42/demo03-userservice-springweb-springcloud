package es.fpdual.store.user.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Product {
    private String name;
    private String description;
    private Double stock;
    private Double price;
    private Category category;
    private Long userId;
}
