package es.fpdual.store.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import es.fpdual.store.user.entity.User;
import es.fpdual.store.user.model.Product;
import es.fpdual.store.user.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<List<User>> listUser() {
        List<User> users = userService.listAllUser();
        if (users.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(users);
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUser(@PathVariable("id") Long id) {
        User user = userService.getUser(id);
        if (user == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(user);
    }

    @PostMapping
    public ResponseEntity<User> createUser(@RequestBody User user) {
        User newUser = userService.createUser(user);
        return ResponseEntity.ok(newUser);
    }

    @GetMapping("/products/{userId}")
    public ResponseEntity<List<Product>> getProducts(@PathVariable("userId") Long userId) {
        User user = userService.getUser(userId);
        if (user == null) {
            return ResponseEntity.notFound().build();
        }

        List<Product> products = userService.getProducts(userId);
        return ResponseEntity.ok(products);
    }

    @PostMapping("/product/{userId}")
    public ResponseEntity<Product> createProduct(@PathVariable("userId") Long userId, @RequestBody Product product) {
        Product newProduct = userService.createProduct(userId, product);
        return ResponseEntity.ok(newProduct);
    }

}
